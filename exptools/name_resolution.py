from fuzzywuzzy import fuzz


def get_close_name(name, possible_names, threshold=80):
    if name is None:
        return None

    name_scores = [(fuzz.ratio(name, n), n) for n in possible_names]
    name_scores.sort()

    ratio, proposition = name_scores[-1]

    if ratio == 100:
        return proposition
    elif ratio > threshold:
        print(rf"'{name}' not found. Using '{proposition}' instead")
        return proposition
    else:
        raise RuntimeError(
            rf"'{name}' not found. Possibly your meant '{name_scores[-1][1]}' or '{name_scores[-2][1]}'"
        )


def find_experiment_tag(database, tag_or_alias, threshold=80):
    if not database:
        return tag_or_alias

    propositions = []
    for exp_tag, aliases in database.items():
        propositions.append((fuzz.ratio(tag_or_alias, exp_tag), exp_tag, exp_tag))
        for alias in aliases:
            propositions.append((fuzz.ratio(tag_or_alias, alias), exp_tag, alias))

    propositions = sorted(propositions, key=lambda x: x[0])
    ratio, proposition, matched = propositions[-1]

    # If no match in database, assume it's just a proper tag and return it.
    if ratio <= threshold or tag_or_alias == proposition:
        return tag_or_alias

    print(
        f"Matched '{tag_or_alias}' with '{matched}', which is mapped to '{proposition}'"
    )
    return proposition
