import pandas as pd


def mean_std_aggregator(df_grouped):
    mean = df_grouped.mean()
    std = df_grouped.std()

    df = pd.DataFrame(
        {"y": mean, "y_lower": mean - std, "y_upper": mean + std}
    ).reset_index()
    return df


def median_min_max_aggregator(df_grouped):
    df = pd.DataFrame(
        {
            "y": df_grouped.median(),
            "y_lower": df_grouped.min(),
            "y_upper": df_grouped.max(),
        }
    ).reset_index()
    return df


def naive_stderr_aggregator(df_grouped):
    mean = df_grouped.mean()
    std = df_grouped.std()
    count = df_grouped.size()

    df = pd.DataFrame(
        {
            "y": mean,
            "y_lower": mean - std / (count ** 0.5),
            "y_upper": mean + std / (count ** 0.5),
        }
    ).reset_index()
    return df
