import functools
import logging
import pathlib
import random
import time
import warnings
from multiprocessing.pool import ThreadPool
from typing import Callable, Dict, List, Union

import neptune
import pandas as pd

from exptools.name_resolution import find_experiment_tag
from exptools.utils import (flatten_dict, get_float_channels,
                            get_neptune_api_token)


class Downloader(object):
    def __init__(
        self,
        organization: str,
        project: str,
        api_token: str = None,
        pool_size: int = 20,
        storage_path: str = None,
        default_database: Dict[str, List[str]] = None,
    ):
        """Downloader class, which keeps all the context needed to download data.

        Args:
            organization: neptune organization name.
            project: neptune project name.
            api_token: neptune api token. If None, NEPTUNE_API_TOKEN env variable will be used.
            pool_size: number of workers to download data.
            storage_path: if provided, will be used as directory for caching data.
            default_database: optional dict of the form {experiment_tag: aliases_list}.
                If provided, alias can be later used instead of neptune tag to identify groups of experiments.
        """
        self.api_token = api_token or get_neptune_api_token()
        self.project_name = organization + "/" + project
        self.pool_size = pool_size
        self.storage_path = storage_path
        self.default_database = default_database

        # Silence spam from Neptune.
        logging.getLogger("neptune-client").setLevel(logging.ERROR)

    def get_data(
        self,
        tags: Union[str, List[str]],
        channel_names: Union[List[str], Callable[[str], bool]] = None,
        parameter_names: List[str] = None,
        get_properties: bool = False,
        invalidate_storage: bool = False,
        database: Dict[str, List[str]] = None,
    ) -> pd.DataFrame:
        """Downloads data from all experiments belonging to given tags and returns a joint DataFrame.

        Args:
            tags: either single (experiment tag or alias), or a list of those
            channel_names: either a list of channels to download, or a filter function that takes channel name (str)
                and returns whether to download it or not (bool).
                Although optional, it is crucial for performance! (sometimes can be 10x faster with this specified)
            parameter_names: optional list of parameters to include in data. On default, all parameters are included.
            get_properties: if True, neptune properties will be downloaded as additional columns.
            invalidate_storage: if True, do not use cached data even if present.
            database: optional dict of the form {experiment_tag: aliases_list}.
                If provided, alias can be later used instead of neptune tag to identify groups of experiments.

        Returns:
            DataFrame containing all the data.
        """
        database = database or self.default_database
        if isinstance(tags, str):
            tags = [tags]

        data = []
        all_experiment_ids = set([])

        if channel_names is None:
            warnings.warn("For better performance, please provide channel_names!")

        for tag_or_alias in tags:
            experiment_tag = find_experiment_tag(database, tag_or_alias)

            experiments = Downloader._get_experiments(
                api_token=self.api_token,
                project_name=self.project_name,
                tag=experiment_tag,
            )

            assert not experiments.empty, f"No data for tag {tag_or_alias}!"

            experiment_ids = list(experiments["sys/id"])

            if all_experiment_ids & set(experiment_ids):
                warnings.warn(
                    "Some experiments are in multiple tags. You will have some duplicated data!"
                )
            all_experiment_ids.update(experiment_ids)

            if self.storage_path:
                tag_storage_path = pathlib.Path(self.storage_path) / experiment_tag
                tag_storage_path.mkdir(parents=True, exist_ok=True)
                cache_path = tag_storage_path / "experiment_data.pkl"
            else:
                cache_path = None

            if cache_path and not invalidate_storage and cache_path.exists():
                tag_data = pd.read_pickle(cache_path)
            else:
                tag_data = self._get_data_multi_process(
                    experiment_ids,
                    channel_names=channel_names,
                    parameter_names=parameter_names,
                    get_properties=get_properties,
                )
                if cache_path:
                    tag_data.to_pickle(cache_path)

            tag_data["experiment_tag"] = tag_or_alias

            data.append(tag_data)

        data = pd.concat(data, ignore_index=True)
        return data

    def _get_data_multi_process(
        self, experiment_ids, channel_names, parameter_names, get_properties
    ):
        start_time = time.time()

        if self.pool_size == 1:
            args = [
                functools.partial(
                    Downloader._get_experiments,
                    api_token=self.api_token,
                    project_name=self.project_name,
                    id=experiment_ids,
                ),
                channel_names,
                parameter_names,
                get_properties,
                self.project_name,
                self.api_token,
            ]

            dfs = Downloader._get_data_single_process(*args)
        else:
            random.shuffle(experiment_ids)

            num_experiments = len(experiment_ids)
            chunk_size = max(int(num_experiments / self.pool_size), 1)
            argssplit = [
                [
                    functools.partial(
                        Downloader._get_experiments,
                        api_token=self.api_token,
                        project_name=self.project_name,
                        id=experiment_ids[x : x + chunk_size],
                    ),
                    channel_names,
                    parameter_names,
                    get_properties,
                    self.project_name,
                    self.api_token,
                ]
                for x in range(0, num_experiments, chunk_size)
            ]

            with ThreadPool(self.pool_size) as p:
                data = p.starmap(Downloader._get_data_single_process, argssplit)

                dfs = []
                for d in data:
                    dfs.extend(d)

        print("Time:", time.time() - start_time)

        return pd.concat(dfs, ignore_index=True)

    @staticmethod
    def _get_data_single_process(
        get_experiments_fn,
        channel_names,
        parameter_names,
        get_properties,
        project_name,
        api_token,
    ):
        data = []
        if channel_names is None:
            channel_names = lambda _: True
        elif isinstance(channel_names, list):
            channel_list = channel_names
            channel_names = lambda ch: ch in channel_list
        experiments = get_experiments_fn()

        for experiment in experiments["sys/id"]:
            run = neptune.init_run(
                project=project_name,
                with_id=experiment,
                api_token=api_token,
                mode="read-only",
                capture_stdout=False,
                capture_stderr=False,
                capture_hardware_metrics=False,
                capture_traceback=False,
                fail_on_exception=False,
            )

            if (
                "sys/trashed" in flatten_dict(run.get_structure()).keys()
                and run["sys/trashed"].fetch()
            ):
                continue

            try:
                float_channels = get_float_channels(run)
                channels = list(filter(channel_names, float_channels))
                experiment_data = Downloader._get_data_for_channels(run, channels)
            except Exception as e:
                print("error downloading data from:", experiment, " skipping")
                print("Exception:", e)
                continue
            experiment_data["experiment_id"] = run["sys/id"].fetch()
            experiment_data["experiment_failed"] = run["sys/failed"].fetch()

            for p, v in flatten_dict(run.get_structure()["parameters"]).items():
                if parameter_names and p not in parameter_names:
                    continue
                experiment_data[p] = v.fetch()

            if get_properties:
                for p, v in flatten_dict(
                    {"properties": run.get_structure()["properties"]}
                ).items():
                    experiment_data[p] = v.fetch()

            data.append(experiment_data)

            run.stop()

        return data

    @staticmethod
    def _get_data_for_channels(run, channels):
        data = []
        for channel in channels:
            channel_run = (
                run[f"logs/{channel}"]
                if "logs" in run.get_structure().keys()
                else run[channel]
            )
            channel_data = channel_run.fetch_values(include_timestamp=False)
            channel_data.set_index("step", inplace=True)
            channel_data.rename(columns={"value": channel}, inplace=True)
            data.append(channel_data)

        data = pd.concat(data, axis=1)
        data = data.reset_index()
        return data

    @staticmethod
    def _get_experiments(api_token, project_name, id=None, tag=None):
        project = neptune.init_project(project=project_name, api_token=api_token)

        return project.fetch_runs_table(id=id, tag=tag).to_pandas()
