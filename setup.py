from setuptools import find_packages, setup

setup(
    name="experiment tools",
    version="0.0.1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "multiprocess",
        "neptune-client",
        "bokeh",
        "fuzzywuzzy",
        "python-Levenshtein",
    ],
)
