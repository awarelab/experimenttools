import seaborn as sns
from bokeh.io import output_file
from exptools.plotter import lineplot

output_file("/tmp/example_plot.html")

dots = sns.load_dataset("dots").query("align == 'dots'")
lineplot(
    dots, x="time", y="firing_rate", split_curves_by="coherence", x_ticks=[0, 100, 200]
)
